//
//  Timer.swift
//  SudokuEC
//
//  Created by Eddie Draaisma on 27/01/2019.
//  Copyright © 2019 Draaisma. All rights reserved.
//

import Foundation

func printTimeElapsedWhenRunningCode(_ operation:() -> ()) {
    let startTime = CFAbsoluteTimeGetCurrent()
    operation()
    let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
    print("Time elapsed : \(timeElapsed)s")
}

